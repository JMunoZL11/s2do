﻿using Shi2do.Models;
using Shi2do.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shi2do.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Summary : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private List<Expense> _expenses;
        private List<Income> _incomes;
        private decimal _totalBalance;
        private decimal _monthBalance;

        public Summary()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await LoadData();

            base.OnAppearing();
        }

        private async Task LoadData()
        {
            await GetTotalBalance();

            GetMonthBalance();
        }

        private decimal GetTotalExpenses(List<Expense> expenses)
        {
            decimal totalExpenses = 0;

            foreach (var expense in expenses)
                totalExpenses = totalExpenses + expense.Value;

            return totalExpenses;
        }
        
        private decimal GetTotalIncomes(List<Income> incomes)
        {
            decimal totalIncomes = 0;

            foreach (var income in incomes)
                totalIncomes = totalIncomes + income.Value;

            return totalIncomes;
        }

        private async Task GetTotalBalance()
        {
            await _connection.CreateTableAsync<Expense>();
            await _connection.CreateTableAsync<Income>();

            var expenses = await _connection.Table<Expense>().ToListAsync();

            _expenses = new List<Expense>(expenses);

            var incomes = await _connection.Table<Income>().ToListAsync();

            _incomes = new List<Income>(incomes);

            _totalBalance = GetTotalIncomes(_incomes) - GetTotalExpenses(_expenses);

            totalBalanceLabel.Text = $"Total Balance: {String.Format("{0:N}", _totalBalance)} AUD$";
        }

        private void GetMonthBalance()
        {
            var dateNow = DateTime.Now;
            var firstDayOfMonth = new DateTime(dateNow.Year, dateNow.Month, 1);

            _expenses = _expenses
                .FindAll(x => x.RegistrationDate <= dateNow && x.RegistrationDate >= firstDayOfMonth);

            _incomes = _incomes
                .FindAll(x => x.RegistrationDate <= dateNow && x.RegistrationDate >= firstDayOfMonth);

            var totalExpenses = _expenses.Count >= 1 ? GetTotalExpenses(_expenses) : 0;

            var totalIncomes = _incomes.Count >= 1 ? GetTotalIncomes(_incomes) : 0;

            _monthBalance = totalIncomes - totalExpenses;

            monthDataLabel.Text = $"Month Balance: {String.Format("{0:N}", totalIncomes)} AUD$ - {String.Format("{0:N}", totalExpenses)} AUD$";
            monthBalanceLabel.Text = $"= {String.Format("{0:N}", _monthBalance)} AUD$";
        }
    }
}