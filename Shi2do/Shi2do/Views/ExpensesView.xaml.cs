﻿
using Shi2do.Models;
using Shi2do.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shi2do.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpensesView : ContentPage
    {
        private List<Expense> _expenses;
        private SQLiteAsyncConnection _connection;

        public ExpensesView()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await LoadData();

            base.OnAppearing();
        }

        private async Task LoadData()
        {
            await _connection.CreateTableAsync<Expense>();

            var dateNow = DateTime.Now;
            var firstDayOfMonth = new DateTime(dateNow.Year, dateNow.Month, 1);

            var expenses = await _connection.Table<Expense>()
                .Where(x => x.RegistrationDate <= dateNow && x.RegistrationDate >= firstDayOfMonth).ToListAsync();

            _expenses = new List<Expense>(expenses);

            expensesList.ItemsSource = _expenses;

            if (_expenses.Count < 1)
            {
                labelList.IsVisible = true;
                labelTotal.IsVisible = false;
                return;
            }

            labelList.IsVisible = false;
            labelTotal.IsVisible = true;
            await GetTotalExpenses();
        }

        private async void OnAddExpense(object sender, System.EventArgs e)
        {
            var page = new ExpenseFormView();

            page.ExpenseAdded += (source, expense) =>
            {
                _expenses.Add(expense);
            };

            await Navigation.PushAsync(page);
        }

        private async void OnDeleteExpense(object sender, System.EventArgs e)
        {
            var expense = (sender as MenuItem).CommandParameter as Expense;

            if (await DisplayAlert("Warning", $"Are you sure you want to delete {expense.Title}?", "Yes", "No"))
            {
                _expenses.Remove(expense);
                await _connection.DeleteAsync(expense);
                await LoadData();
            }
        }

        private async Task GetTotalExpenses()
        {
            decimal totalExpenses = 0;

            var expenses = await _connection.Table<Expense>().ToListAsync();

            foreach (var expense in expenses)
                totalExpenses = totalExpenses + expense.Value;

            labelTotal.Text = $"Total expenses: {String.Format("{0:N}", totalExpenses)} AUD$";
        }
    }
}