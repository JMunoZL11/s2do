﻿using Shi2do.Models;
using Shi2do.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shi2do.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncomeFormView : ContentPage
    {
        private SQLiteAsyncConnection _connection;

        public event EventHandler<Income> IncomeAdded;

        public IncomeFormView()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        private async void OnSaveIncome(object sender, EventArgs e)
        {
            if (InvalidFields())
            {
                await DisplayAlert("Error", "All fields are required", "OK");
                return;
            }

            var Income = new Income
            {
                Title = titleEntry.Text,
                Value = decimal.Parse(valueEntry.Text, CultureInfo.InvariantCulture),
                RegistrationDate = DateTime.Now
            };

            await _connection.InsertAsync(Income);

            IncomeAdded?.Invoke(this, Income);

            await Navigation.PopAsync();
        }

        private bool InvalidFields()
        {
            if (String.IsNullOrWhiteSpace(titleEntry.Text))
                return true;

            if (String.IsNullOrWhiteSpace(valueEntry.Text))
                return true;

            return false;
        }
    }
}