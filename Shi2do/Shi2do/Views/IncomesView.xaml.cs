﻿using Shi2do.Models;
using Shi2do.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shi2do.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncomesView : ContentPage
    {
        private List<Income> _incomes;
        private SQLiteAsyncConnection _connection;

        public IncomesView()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            await LoadData();

            base.OnAppearing();
        }

        private async Task LoadData()
        {
            await _connection.CreateTableAsync<Income>();

            var dateNow = DateTime.Now;
            var firstDayOfMonth = new DateTime(dateNow.Year, dateNow.Month, 1);

            var Incomes = await _connection.Table<Income>()
                .Where(x => x.RegistrationDate <= dateNow && x.RegistrationDate >= firstDayOfMonth).ToListAsync();

            _incomes = new List<Income>(Incomes);

            incomesList.ItemsSource = _incomes;

            if (_incomes.Count < 1)
            {
                labelList.IsVisible = true;
                labelTotal.IsVisible = false;
                return;
            }

            labelList.IsVisible = false;
            labelTotal.IsVisible = true;
            await GetTotalIncomes();
        }

        private async void OnAddIncome(object sender, System.EventArgs e)
        {
            var page = new IncomeFormView();

            page.IncomeAdded += (source, Income) =>
            {
                _incomes.Add(Income);
            };

            await Navigation.PushAsync(page);
        }

        private async void OnDeleteIncome(object sender, System.EventArgs e)
        {
            var Income = (sender as MenuItem).CommandParameter as Income;

            if (await DisplayAlert("Warning", $"Are you sure you want to delete {Income.Title}?", "Yes", "No"))
            {
                _incomes.Remove(Income);
                await _connection.DeleteAsync(Income);
                await LoadData();
            }
        }

        private async Task GetTotalIncomes()
        {
            decimal totalIncomes = 0;

            var incomes = await _connection.Table<Income>().ToListAsync();

            foreach (var income in incomes)
                totalIncomes = totalIncomes + income.Value;

            labelTotal.Text = $"Total Incomes: {String.Format("{0:N}",totalIncomes)} AUD$";
        }
    }
}