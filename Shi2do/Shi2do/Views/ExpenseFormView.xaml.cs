﻿using Shi2do.Models;
using Shi2do.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Shi2do.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpenseFormView : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private IList<Category> _categories;
        private Category _categorySelected;

        public event EventHandler<Expense> ExpenseAdded;

        public ExpenseFormView()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        protected override async void OnAppearing()
        {
            _categories = await LoadCategories();

            foreach (var category in _categories)
                categoriesPicker.Items.Add(category.Name);

            base.OnAppearing();
        }

        private async void OnSaveExpense(object sender, EventArgs e)
        {
            if (InvalidFields())
            {
                await DisplayAlert("Error", "All fields are required", "OK");
                return;
            }

            var expense = new Expense
            {
                Title = titleEntry.Text,
                Value = decimal.Parse(valueEntry.Text, CultureInfo.InvariantCulture),
                RegistrationDate = DateTime.Now,
                CategoryId = _categorySelected.Id
            };

            await _connection.InsertAsync(expense);

            ExpenseAdded?.Invoke(this, expense);

            await Navigation.PopAsync();
        }

        private void Category_selected(object sender, EventArgs e)
        {
            var name = categoriesPicker.Items[categoriesPicker.SelectedIndex];
            _categorySelected = _categories.Single(c => c.Name == name);
        }

        private async Task<IList<Category>> LoadCategories()
        {
            await _connection.CreateTableAsync<Category>();

            return await _connection.Table<Category>().ToListAsync();

        }

        private bool InvalidFields()
        {
            if (String.IsNullOrWhiteSpace(titleEntry.Text))
                return true;

            if (String.IsNullOrWhiteSpace(valueEntry.Text))
                return true;

            if (_categorySelected == null)
                return true;

            if (String.IsNullOrWhiteSpace(_categorySelected.Id.ToString()))
                return true;

            return false;
        }
    }
}