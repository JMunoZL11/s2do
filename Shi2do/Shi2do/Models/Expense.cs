﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;

namespace Shi2do.Models
{
    public class Expense
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Title { get; set; }
        public decimal Value { get; set; }
        public DateTime RegistrationDate { get; set; }
        [ForeignKey(typeof(Category))]
        public int CategoryId { get; set; }
        [ManyToOne]
        public Category Category { get; set; }
    }
}
