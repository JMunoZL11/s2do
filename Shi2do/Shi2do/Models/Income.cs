﻿using SQLite;
using System;

namespace Shi2do.Models
{
    public class Income
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Title { get; set; }
        public decimal Value { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}
