﻿using Shi2do.Droid;
using Shi2do.Persistence;
using SQLite;
using System;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteDb))]

namespace Shi2do.Droid
{
    public class SQLiteDb : ISQLiteDb
	{
		public SQLiteAsyncConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var path = Path.Combine(documentsPath, "Shi2doSQLite.db3");

			return new SQLiteAsyncConnection(path);
		}
	}
}

